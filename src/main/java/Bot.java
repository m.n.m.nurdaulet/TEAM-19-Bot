import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Bot extends TelegramLongPollingBot {
    int a = 0;
    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new Bot());

        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }


    public void sendMsg(Message message, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);

        sendMessage.setChatId(message.getChatId().toString());

        sendMessage.setReplyToMessageId(message.getMessageId());

        sendMessage.setText(text);
        try {

            setButtons(sendMessage);
            sendMessage(sendMessage);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    public void onUpdateReceived(Update update) {
        Model model = new Model();
        Message message = update.getMessage();
        if (message != null && message.hasText()) {
            switch (message.getText()) {
                case "/start":
                    sendMsg(message, "Здравствуйте! Я Бот группы No19, могу показывать погоду во всех городах, a также расписание учителей");
                    break;
                case "Выйти с Погоды":
                    a = 0;
                    sendMsg(message, "Переходим в главное меню");
                    break;
                case "Расписание Учителя":
                    a = 3;
                    sendMsg(message, "Выберите группу");
                    break;
                case "Выйти":
                    a = 0;
                    sendMsg(message, "Переходим в главное меню");
                    break;
                case "John Doe": sendMsg(message, "Понедельник: " +
                        "10:00 - 11:50 IT-1905." +
                        "14:00 - 15:50 IT-1902" +
                        "16:00  - 17:50 CS-1903" +
                        "Вторник: " +
                        "10:00 - 11:50 IT-1905." +
                        "14:00 - 15:50 IT-1902" +
                        "16:00  - 17:50 CS-1903" +
                        "Среда: " +
                        "10:00 - 11:50 IT-1905." +
                        "14:00 - 15:50 IT-1902" +
                        "16:00  - 17:50 CS-1903" +
                        "Четверг: " +
                        "10:00 - 11:50 IT-1905." +
                        "14:00 - 15:50 IT-1902" +
                        "16:00  - 17:50 CS-1903" +
                        "Пятница: " +
                        "10:00 - 11:50 IT-1905." +
                        "14:00 - 15:50 IT-1902" +
                        "16:00  - 17:50 CS-1903"
                );
                    sendMsg(message, "Данные являются не подлинными, вследствии конца первого триместра");
                case "Moldabekov Akhat":
                    sendMsg(message, "Понедельник: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903" +
                            "Вторник: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903" +
                            "Среда: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903" +
                            "Четверг: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903" +
                            "Пятница: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903"
                    );
                    sendMsg(message, "Данные являются не подлинными, вследствии конца первого триместра");
                case "Tulzhanov Olzhas":
                    sendMsg(message, "Понедельник: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903" +
                            "Вторник: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903" +
                            "Среда: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903" +
                            "Четверг: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903" +
                            "Пятница: " +
                            "10:00 - 11:50 IT-1905." +
                            "14:00 - 15:50 IT-1902" +
                            "16:00  - 17:50 CS-1903"
                    );
                    sendMsg(message, "Данные являются не подлинными, вследствии конца первого триместра");
                default:
                    if (message.getText().equals("Погода")) {
                        sendMsg(message, "Выберите город");
                        a = 2;
                    }
                   else  if (a == 2) {
                        System.out.print("Hello");
                        try {
                            sendMsg(message, Weather.getWeather(message.getText(), model));
                        } catch (IOException e) {
                            sendMsg(message, "Город не найден!");
                        }
                    }
                    break;
            }
        }

    }


    public void setButtons(SendMessage sendMessage) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboardRowList = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        KeyboardRow keyboardSRow = new KeyboardRow();

        if(a == 0) {
            keyboardFirstRow.add(new KeyboardButton("Погода"));
            keyboardFirstRow.add(new KeyboardButton("Расписание Учителя"));
        } else if(a == 2){
            keyboardFirstRow.add(new KeyboardButton("Выйти с Погоды"));
        } else if(a == 3){
            keyboardFirstRow.add(new KeyboardButton("John Doe"));
            keyboardRowList.add(keyboardFirstRow);
            keyboardFirstRow = new KeyboardRow();
            keyboardFirstRow.add(new KeyboardButton("Moldabekov Akhat"));
            keyboardRowList.add(keyboardFirstRow);
            keyboardFirstRow = new KeyboardRow();
            keyboardFirstRow.add(new KeyboardButton("Tulzhanov Olzhas"));
            keyboardRowList.add(keyboardFirstRow);
            keyboardFirstRow = new KeyboardRow();
            keyboardFirstRow.add(new KeyboardButton("Выйти"));
        }
        keyboardRowList.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard(keyboardRowList);
    }

    public String getBotUsername() {
        return "AITU_TEAM_19";
    }

    public String getBotToken() {
        return "1040379219:AAHbhkSKCpeZsZo4-cCgjFGdMePpi_PX8BA";
    }
}
